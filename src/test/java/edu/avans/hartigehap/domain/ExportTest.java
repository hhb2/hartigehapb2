package edu.avans.hartigehap.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExportTest {

	private Bill bill;
	private Order order;
	private OrderItem orderItem;
	private OrderItem orderItem2;
	private MenuItem menuItem;
	private MenuItem menuItem2;
	private List<FoodCategory> foodCats = new ArrayList<FoodCategory>();
	private FoodCategory fc;
	private static final long serialVersionUID = 1L;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		fc = new FoodCategory("tag");
		foodCats.add(fc);
		bill = new Bill();
		bill.setId(serialVersionUID);
		order = new Order();
		menuItem = new Drink("Bier", "beer.jpg", 2, Arrays.<FoodCategory>asList(new FoodCategory[]{foodCats.get(0)}), Drink.Size.LARGE);
		menuItem2 = new Drink("Pasta", "beer.jpg", 11, Arrays.<FoodCategory>asList(new FoodCategory[]{foodCats.get(0)}), Drink.Size.LARGE);
		orderItem = new OrderItem(menuItem, 30);
		orderItem2 = new OrderItem(menuItem2, 2);
		Collection<OrderItem> col = new ArrayList<OrderItem>();
		col.add(orderItem);
		col.add(orderItem2);

		Collection<Order> col2 = new ArrayList<Order>();
		col2.add(order);

		order.setBill(bill);
		order.setOrderItems(col);
		bill.setOrders(col2);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		bill.export("file");
	}

}
