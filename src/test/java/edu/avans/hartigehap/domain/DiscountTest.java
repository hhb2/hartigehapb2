package edu.avans.hartigehap.domain;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DiscountTest {

	private Discount discount;
	private Bill bill;
	private Order order;
	private OrderItem orderItem;
	private OrderItem orderItem2;
	private OrderItem orderItem3;
	private MenuItem menuItem;
	private MenuItem menuItem2;
	private List<FoodCategory> foodCats = new ArrayList<FoodCategory>();
	private FoodCategory fc;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Calendar calStart = Calendar.getInstance();
		calStart.set(2015, Calendar.JANUARY, 1);
		Calendar calEnd = Calendar.getInstance();
		calEnd.set(2015, Calendar.JUNE, 1);
		discount = new Discount("10 procent op alles", calStart.getTime(), calEnd.getTime(), 0.9);
		fc = new FoodCategory("tag");
		foodCats.add(fc);
		bill = new Bill();
		order = new Order();
		menuItem = new Drink("Bier", "beer.jpg", 2,
				Arrays.<FoodCategory> asList(new FoodCategory[] { foodCats
						.get(0) }), Drink.Size.LARGE);
		menuItem2 = new Drink("Pasta", "beer.jpg", 11,
				Arrays.<FoodCategory> asList(new FoodCategory[] { foodCats
						.get(0) }), Drink.Size.LARGE);
		orderItem = new OrderItem(menuItem, 30);
		orderItem2 = new OrderItem(menuItem, 3);
		orderItem3 = new OrderItem(menuItem2, 2);
		Collection<OrderItem> col = new ArrayList<OrderItem>();
		col.add(orderItem);
		col.add(orderItem2);
		col.add(orderItem3);

		Collection<Order> col2 = new ArrayList<Order>();
		col2.add(order);

		order.setBill(bill);
		order.setOrderItems(col);
		bill.setOrders(col2);
		bill.submit();
}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		double noDis = bill.getPriceAllOrders();
		System.out.println("Without discount: " + bill.getPriceAllOrders());
		bill.setDiscount(discount);
		System.out.println("With discount: " + bill.getPriceAllOrders());
		double withDis = bill.getPriceAllOrders();
		assertTrue(noDis > withDis);
	}

}
