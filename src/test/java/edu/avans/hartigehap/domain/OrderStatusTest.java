package edu.avans.hartigehap.domain;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class OrderStatusTest {
	Order order;

	public OrderStatusTest() {
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		order = new Order();
		order.addOrderItem(null);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreatedStatus() {
		assertTrue(order.getOrderStatus().getOrderStatusId() == OrderStatus.OrderStatusId.CREATED);
	}

	@Test
	public void testSubmittedStatus() {
		try {
			order.submit();
		} catch (Exception e) {
			System.out.println(e);
			fail();
		}
		assertTrue(order.getOrderStatus().getOrderStatusId() == OrderStatus.OrderStatusId.SUBMITTED);
	}

	@Test
	public void testAcceptedStatus() {
		try {
			order.submit();
			order.plan();
		} catch (Exception e) {
			System.out.println(e);
			fail();
		}
		assertTrue(order.getOrderStatus().getOrderStatusId() == OrderStatus.OrderStatusId.PLANNED);
	}

	@Test
	public void testPreparedStatus() {
		try {
			order.submit();
			order.plan();
			order.prepare();
		} catch (Exception e) {
			System.out.println(e);
			fail();
		}
		assertTrue(order.getOrderStatus().getOrderStatusId() == OrderStatus.OrderStatusId.PREPARED);
	}

	@Test
	public void testServedStatus() {
		try {
			order.submit();
			order.plan();
			order.prepare();
			order.serve();
		} catch (Exception e) {
			System.out.println(e);
			fail();
		}
		assertTrue(order.getOrderStatus().getOrderStatusId() == OrderStatus.OrderStatusId.SERVED);
	}

	@Test
	public void testServedStatus2() {
		boolean thrown = false;
		try {
			order.submit();
			order.plan();
			order.prepare();
			order.serve();
			order.serve();
		} catch (Exception e) {
			thrown = true;
		}
		assertTrue(thrown);
	}
}