package edu.avans.hartigehap.domain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DecoratorTest {
	private List<FoodCategory> foodCats = new ArrayList<FoodCategory>();
	private FoodCategory fc;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		fc = new FoodCategory("tag");
		foodCats.add(fc);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSimplePizza() {
		Pizza pizza = new SimplePizza("normaal", "pizza.jpg", 5 , foodCats, "recipe");
		Pizza pizza2 = new SimplePizza("calzone", "pizza.jpg", 7, foodCats, "recipe");
		
		assertEquals(pizza.getId(), "normaal");
		assertEquals(pizza.getPrice(), 5, 0.0);
		assertEquals(pizza.getFoodCategories(), foodCats);
		assertEquals(pizza2.getId(), "calzone");
		assertEquals(pizza2.getPrice(), 7, 0.0);
		assertEquals(pizza2.getFoodCategories(), foodCats);
	}
	
	@Test
	public void testDecoratedPizza(){
		Topping tonno = new Topping("Tonno", 2.5);
		Topping funghi = new Topping("Funghi", 2);
		Topping mozzarella = new Topping("Mozzarella", 1);
		Topping shoarma = new Topping("Shoarma", 4.0);
		Topping knoflook = new Topping("Knoflook", 0.5);
		Pizza simplePizza = new SimplePizza("normale pizza", "pizza.jpg", 5, foodCats, "recipe");
		Meal decPizza = new Decorator(tonno, new Decorator(funghi, new Decorator(mozzarella, simplePizza)));
		Meal decPizza2 = new Decorator(shoarma, new Decorator(knoflook, simplePizza));
		
		assertEquals(decPizza.getId(), "normale pizza Mozzarella Funghi Tonno");
		assertEquals(decPizza.getPrice(), 10.5, 0.0);
		assertEquals(decPizza.getImageFileName(), "pizza.jpg");
		assertEquals(decPizza.getFoodCategories(), foodCats);
		assertEquals(decPizza.getRecipe(), "recipe");
		assertEquals(decPizza2.getId(), "normale pizza Knoflook Shoarma");
		assertEquals(decPizza2.getPrice(), 9.5, 0.0);
		assertEquals(decPizza2.getImageFileName(), "pizza.jpg");
		assertEquals(decPizza2.getFoodCategories(), foodCats);
		assertEquals(decPizza2.getRecipe(), "recipe");
		
		
	}

}
