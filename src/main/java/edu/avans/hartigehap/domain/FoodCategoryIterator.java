package edu.avans.hartigehap.domain;

import java.util.Iterator;
import java.util.List;

public class FoodCategoryIterator implements Iterator<FoodCategory>{

	
	List<FoodCategory> foodCategories;
	int position = 0;
	
	public FoodCategoryIterator(List<FoodCategory> foodCategories){
		this.foodCategories = foodCategories;
	}
	
	
	@Override
	public boolean hasNext() {
		if (position >= foodCategories.size() || foodCategories.get(position) == null){
			return false;
		} else {
			return true;
		}
	}

	@Override
	public FoodCategory next() {
		FoodCategory foodCategory = foodCategories.get(position);
		position = position +1;
		return foodCategory;
	}

}
