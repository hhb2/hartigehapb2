package edu.avans.hartigehap.domain;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Decorator extends PizzaDecorator{
	private static final long serialVersionUID = 1L;

	private Topping topping;
	public Decorator(Topping topping, Pizza pizzaToBeDecorated) {
		super(pizzaToBeDecorated);
		this.topping = topping;
	}
	
	@Override
	public String getId(){
		return super.getId() + " " + topping.getName();
	}
	
	@Override
	public double getPrice(){
		return super.getPrice() + topping.getPrice();
	}

}
