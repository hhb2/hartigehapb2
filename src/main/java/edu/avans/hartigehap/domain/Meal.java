package edu.avans.hartigehap.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
public class Meal extends MenuItem implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String recipe;

	public Meal() {

	}

	public Meal(String id, String imageFileName, double price,
			Collection<FoodCategory> foodCategories, String recipe) {
		super(id, imageFileName, price, foodCategories);
		this.recipe = recipe;
	}

}