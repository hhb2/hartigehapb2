package edu.avans.hartigehap.domain;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;

public class ExportFile implements ExportStrategy {

	@Override
	public void export(Bill bill) {
		String newline = System.getProperty("line.separator");
		ArrayList<String> toPrint = new ArrayList<String>();
		Iterator<Order> it = bill.getOrders().iterator();
		while (it.hasNext()) {
			Iterator<OrderItem> it2 = it.next().getOrderItems().iterator();
			while (it2.hasNext()) {
				OrderItem oi = it2.next();
				String name = oi.getMenuItem().getId();
				int quantity = oi.getQuantity();
				double price = oi.getPrice();
				// SIZE toevoegen
				toPrint.add(quantity + "x " + name + " €" + price + newline);
			}
		}

		Writer writer = null;

		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("Bill-" + bill.getId() + ".txt"),
					"utf-8"));
			for (String line : toPrint) {
				writer.write(line);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				writer.write("Total: €" + bill.getPriceAllOrders());
				writer.close();
			} catch (Exception ex) {
			}
		}
	}
}
