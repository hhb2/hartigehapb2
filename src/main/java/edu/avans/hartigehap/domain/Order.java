package edu.avans.hartigehap.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * 
 * @author B100
 */
@Entity
@NamedQuery(name = "Order.findSubmittedOrders", query = "SELECT o FROM Order o "
		+ "WHERE o.orderStatus.orderStatusId = edu.avans.hartigehap.domain.OrderStatus$OrderStatusId.SUBMITTED "
		+ "AND o.bill.diningTable.restaurant = :restaurant "
		+ "ORDER BY o.submittedTime")
// to prevent collision with MySql reserved keyword
@Table(name = "ORDERS")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ORDER_ID")
	public Long id;
	
	// unidirectional one-to-many relationship.
	@OneToMany(cascade = javax.persistence.CascadeType.ALL)
	// additional control over database column names (optional) 
	@JoinColumn(name = "ERCO_ORDERITEM_ORDER_ID", referencedColumnName = "ORDER_ID")
	private Collection<OrderItem> orderItems = new ArrayList<OrderItem>();
	
	@ManyToOne()
	private Bill bill;

	@Temporal(TemporalType.TIMESTAMP)
	private Date submittedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date plannedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date preparedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date servedTime;

	@OneToOne(cascade = javax.persistence.CascadeType.ALL)
	protected OrderStatus orderStatus;

	public Order() {
		orderStatus = new OrderCreated(this);
	}
	
	public Order(Long id, Collection<OrderItem> orderItems) {
		this.id = id;
		this.orderItems = orderItems;
		orderStatus = new OrderCreated(this);
	}

	/* business logic */

	@Transient
	public boolean isSubmittedOrSuccessiveState() {
		return orderStatus.getOrderStatusId() != OrderStatus.OrderStatusId.CREATED;
	}

	// transient annotation, because methods starting with are recognized by JPA as properties
	@Transient
	public boolean isEmpty() {
		return orderItems.isEmpty();
	}

	public void addOrderItem(MenuItem menuItem) {
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		boolean found = false;
		while (orderItemIterator.hasNext()) {
			OrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				orderItem.incrementQuantity();
				found = true;
				break;
			}
		}
		if (!found) {
			OrderItem orderItem = new OrderItem(menuItem, 1);
			orderItems.add(orderItem);
		}
	}

	public void deleteOrderItem(MenuItem menuItem) {
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		boolean found = false;
		while (orderItemIterator.hasNext()) {
			OrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				found = true;
				if (orderItem.getQuantity() > 1) {
					orderItem.decrementQuantity();
				} else {
					// orderItem.getQuantity() == 1
					orderItemIterator.remove();
				}
				break;
			}
		}
		if (!found) {
			// do nothing
		}
	}

	public void submit() throws StateException {
		orderStatus.submit();
	}

	public void plan() throws StateException {
		orderStatus.plan();
	}

	public void prepare() throws StateException {
		orderStatus.prepare();
	}

	public void serve() throws StateException {
		orderStatus.serve();
	}

	@Transient
	public int getPrice() {
		int price = 0;
		Iterator<OrderItem> orderItemIterator = orderItems.iterator();
		while (orderItemIterator.hasNext()) {
			price += orderItemIterator.next().getPrice();
		}
		return price;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Order)) {
			return false;
		}
		Order other = (Order) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "entity.Order[ id=" + id + " ]";
	}

}