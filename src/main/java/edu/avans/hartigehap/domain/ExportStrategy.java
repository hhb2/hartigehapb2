package edu.avans.hartigehap.domain;

public interface ExportStrategy {
	
	public void export(Bill bill);

}
