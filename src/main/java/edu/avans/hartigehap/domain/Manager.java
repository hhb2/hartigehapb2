package edu.avans.hartigehap.domain;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Manager extends EmployeeComponent {
	private static final long serialVersionUID = 1L;
	private String firstName, lastName, role;
	private Double salary;
	private boolean vaultAccess, restaurantKey;
	@Column(columnDefinition="blob")
	private ArrayList<EmployeeComponent> employeeComponents;

	public Manager(){
		
	}
	
	public Manager(String firstName, String lastName, Double salary, String role, boolean vaultAccess, boolean restaurantKey) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;
		this.role = role;
		this.vaultAccess = vaultAccess;
		this.restaurantKey = restaurantKey;
		employeeComponents = new ArrayList<EmployeeComponent>();
	}
	
	public void add(EmployeeComponent employeeComponent){
		employeeComponents.add(employeeComponent);
	}
	
	public void remove(EmployeeComponent employeeComponent){
		employeeComponents.remove(employeeComponent);
	}
	
	public EmployeeComponent getChild(int i){
		return employeeComponents.get(i);
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public Double getSalary(){
		return salary;
	}
	
	public String getRole(){
		return role;
	}
	
	public boolean hasVaultAccess(){
		return vaultAccess;
	}
	
	public boolean hasRestaurantKey(){
		return restaurantKey;
	}

}
