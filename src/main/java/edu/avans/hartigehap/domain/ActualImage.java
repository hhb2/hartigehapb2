package edu.avans.hartigehap.domain;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import edu.avans.hartigehap.web.util.customBase64;


public class ActualImage implements Image {

	private String imageFileLocationURL = "https://bytebucket.org/ercoargante/hhspring/raw/87bfeedf3953491574afffd1dd18c0b9c59b2c27/src/main/webapp/images/";
	private String imageBase64;
	
	public ActualImage(String fileName){
		loadFromWeb(fileName);
	}
	
	@Override
	public String showImage() {
			return imageBase64;
	}
	
	private void loadFromWeb(String _imageFileName){
		if (_imageFileName != null || _imageFileName != "") {
			try {
				URL imageFileName = new URL(imageFileLocationURL + _imageFileName);			
				InputStream in = new BufferedInputStream(
						imageFileName.openStream());
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				int n = 0;
				while (-1 != (n = in.read(buf))) {
					out.write(buf, 0, n);
				} 
				out.close();
				in.close();
				byte[] response = out.toByteArray();

				StringBuilder sb = new StringBuilder();
				sb.append("data:image/jpg;base64,");
				sb.append(customBase64.encode(response));
				
				imageBase64 =  sb.toString();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
