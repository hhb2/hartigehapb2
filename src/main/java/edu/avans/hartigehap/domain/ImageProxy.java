package edu.avans.hartigehap.domain;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ImageProxy implements Image {

	private ActualImage actualimage;
	private String filename;
	
	public ImageProxy(String filename){
		this.filename = filename;
	}
	
	@Override
	public String showImage() {
		if(actualimage == null){
			actualimage = new ActualImage(filename);
		}
		return actualimage.showImage();
	}
		
}
