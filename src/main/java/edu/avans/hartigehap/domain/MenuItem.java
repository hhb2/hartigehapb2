package edu.avans.hartigehap.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * @author Erco
 */
@Entity
@Table(name = "MENUITEMS")
// images are stored in a separate database table (optional)
@SecondaryTable(name = "MENUITEM_IMAGES", pkJoinColumns = @PrimaryKeyJoinColumn(name = "MENUITEM_ID"))
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
public abstract class MenuItem implements Serializable {
	private static final long serialVersionUID = 1L;

	// use a natural key as primary key instead of a surrogate key (note:
	// no auto generation of primary key needed)
	@Id
	@Column(name = "MENUITEM_ID")
	private String id;
	
	@ManyToMany(cascade = javax.persistence.CascadeType.ALL)
	private Collection<FoodCategory> foodCategories = new ArrayList<FoodCategory>();
	
	// image stored in the database
	@Column(name = "IMAGE", table = "MENUITEM_IMAGES")
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private String image;
	
	// filename of image stored in the database
	@Column(name = "IMAGEFILENAME")
	private String imageFileName; 
	
	// JPA is case sensitive; the corresponding column name will be in small
	// caps: "price"
	private double price;

	public MenuItem() {
	}

	public MenuItem(String id, String imageFileName, double price,
			Collection<FoodCategory> foodCategories) {
		this.id = id;
		this.imageFileName = imageFileName;
		this.price = price;
		this.foodCategories = foodCategories;
		this.image = new ImageProxy(imageFileName).showImage();
	}


	/* business logic */

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof MenuItem)) {
			return false;
		}
		MenuItem other = (MenuItem) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "entity.MenuItem[ id=" + id + " ]";
	}

}