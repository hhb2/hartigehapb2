package edu.avans.hartigehap.domain;

import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public abstract class Pizza extends Meal {
	private static final long serialVersionUID = 1L;
	public Pizza(String id, String imageFileName, double price,
			Collection<FoodCategory> foodCategories, String recipe){
		super(id, imageFileName, price, foodCategories, recipe);
	}
	
	public Pizza(){};
	
	
	

}
