package edu.avans.hartigehap.domain;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public class Employee extends EmployeeComponent {
	private static final long serialVersionUID = 1L;
	private String firstName, lastName, role;
	private Double salary;
	
	public Employee(){
		
	}
	
	public Employee(String firstName, String lastName, Double salary, String role) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.salary = salary;
		this.role = role;
	}

	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public Double getSalary(){
		return salary;
	}
	
	public String getRole(){
		return role;
	}
	
	

}
