package edu.avans.hartigehap.domain;

import java.util.Collection;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Getter @Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public abstract class PizzaDecorator extends Pizza{
	private static final long serialVersionUID = 1L;
	private Pizza pizzaToBeDecorated;
	public PizzaDecorator(Pizza pizzaToBeDecorated) {
		this.pizzaToBeDecorated = pizzaToBeDecorated;
	}
	@Override
	public String getId(){
		return pizzaToBeDecorated.getId();
	}
	@Override
	public String getImageFileName(){
		return pizzaToBeDecorated.getImageFileName();
	}

	@Override
	public double getPrice() {
		return pizzaToBeDecorated.getPrice();
	}
	
	@Override
	public Collection<FoodCategory> getFoodCategories(){
		return pizzaToBeDecorated.getFoodCategories();
	}
	
	@Override
	public String getImage(){
		return pizzaToBeDecorated.getImage();
	}
	
	@Override
	public String getRecipe(){
		return pizzaToBeDecorated.getRecipe();
	}

}