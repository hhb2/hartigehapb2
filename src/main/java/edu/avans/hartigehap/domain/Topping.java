package edu.avans.hartigehap.domain;

import java.io.Serializable;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
public class Topping extends DomainObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public Topping(){
		
	}
	
	public Topping(String name, double price){
		this.name = name;
		this.price = price;
	}
	
	private String name;
	private double price;
	
	public String getName(){
		return name;
	}
	public double getPrice(){
		return price;
	}

	
}
