package edu.avans.hartigehap.domain;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
public class Drink extends MenuItem implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Size size;

	public enum Size {
		SMALL, MEDIUM, LARGE
	}

	public Drink() {

	}

	public Drink(String id, String imageFileName, double price,
			Collection<FoodCategory> foodCategories, Size size) {
		super(id, imageFileName, price, foodCategories);
		this.size = size;

	}

	// business logic
}