package edu.avans.hartigehap.domain;

import javax.persistence.Entity;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
public abstract class EmployeeComponent extends DomainObject{
	private static final long serialVersionUID = 1L;

	public EmployeeComponent() {
	}
	
	public void add(EmployeeComponent employeeComponent){
		throw new UnsupportedOperationException();
	}
	
	public void remove(EmployeeComponent employeeComponent){
		throw new UnsupportedOperationException();
	}
	
	public EmployeeComponent getChild(int i){
		throw new UnsupportedOperationException();
	}
	
	public String getFirstName(){
		throw new UnsupportedOperationException();
	}
	
	public String getLastName(){
		throw new UnsupportedOperationException();
	}
	
	public DateTime getBirthDate(){
		throw new UnsupportedOperationException();
	}
	
	public Double getSalary(){
		throw new UnsupportedOperationException();
	}
	
	public String getRole(){
		throw new UnsupportedOperationException();
	}
	
	public boolean hasVaultAccess(){
		throw new UnsupportedOperationException();
	}
	
	public boolean hasRestaurantKey(){
		throw new UnsupportedOperationException();
	}

}
