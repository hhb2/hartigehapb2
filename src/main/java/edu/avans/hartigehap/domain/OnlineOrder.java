package edu.avans.hartigehap.domain;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
//@NamedQuery(name = "OnlineOrder.findSubmittedOrders", query = "SELECT o FROM OnlineOrder o "
//		+ "WHERE o.orderStatus.orderStatusId = edu.avans.hartigehap.domain.OrderStatus$OrderStatusId.SUBMITTED "
//		+ "ORDER BY o.submittedTime")
@Table(name = "ONLINEORDERS")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter @Setter
public class OnlineOrder extends Order {
	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ONLINEORDER_ID")
	private String street;
	private String city;
	private String zipCode;

	public OnlineOrder() {
		orderStatus = new OrderCreated(this);
	}

	public OnlineOrder(Long id, Collection<OrderItem> orderItems, String street, String city, String zipCode) {
		super(id, orderItems);
		this.street = street;
		this.city = city;
		this.zipCode = zipCode;
		orderStatus = new OrderCreated(this);
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof OnlineOrder)) {
			return false;
		}
		OnlineOrder other = (OnlineOrder) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "entity.OnlineOrder[ id=" + id + " ]";
	}
}
