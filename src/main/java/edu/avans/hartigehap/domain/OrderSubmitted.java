package edu.avans.hartigehap.domain;

import java.util.Date;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id") 
@Getter @Setter
public class OrderSubmitted extends OrderStatus {
	private static final long serialVersionUID = 1L;
	
	public OrderSubmitted(Order order){
		super(order);
		orderStatusId = OrderStatusId.SUBMITTED;
	}
	
	public OrderSubmitted(){
		orderStatusId = OrderStatusId.SUBMITTED;
	}

	@Override
	public void submit() throws StateException{
		throw new StateException(
			"not allowed to submit an order, if it is not in the created state");		
		
	}

	@Override
	public void plan() throws StateException{
		order.setPlannedTime(new Date());
		order.setOrderStatus(new OrderPlanned(getOrder()));
		
	}

	@Override
	public void prepare() throws StateException {
		throw new StateException(
				"not allowed to prepare an order, if it is not in the planned state");
	}

	
	@Override
	public void serve() throws StateException {
		throw new StateException(
				"not allowed to serve an order, if it is not in the prepared state");
	}
}