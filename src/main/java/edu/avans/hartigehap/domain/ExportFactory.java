package edu.avans.hartigehap.domain;

public class ExportFactory {
	private volatile static ExportFactory uniqueInstance;

	private ExportFactory() {

	}

	public static ExportFactory getUniqueInstance() {
		if (uniqueInstance == null) {
			synchronized (ExportFactory.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new ExportFactory();
				}
			}
		}

		return uniqueInstance;
	}
	
	public ExportStrategy getExportType(String type){
		ExportStrategy export = null;
		switch(type){
		case "file": 
			export = new ExportFile();
			break;
		}
		return export;
	}
}
