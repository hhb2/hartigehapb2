package edu.avans.hartigehap.domain;

import java.util.Iterator;
import java.util.List;

public class MenuIterator implements Iterator<MenuItem>{

	List<MenuItem> menuItems;
	int position = 0;
	
	public MenuIterator(List<MenuItem> menuItems){
		this.menuItems = menuItems;
	}
	
	@Override
	public boolean hasNext() {
		if (position >= menuItems.size()){
			return false;
		} else {
			return true;
		}
	}

	@Override
	public MenuItem next() {
		MenuItem menuItem = menuItems.get(position);
		position = position +1;
		return menuItem;
	}

}
