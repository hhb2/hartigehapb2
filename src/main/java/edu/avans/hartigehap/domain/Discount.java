package edu.avans.hartigehap.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//optional
@Entity
@Table(name = "DISCOUNT")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id") 
@Getter @Setter
public class Discount extends DomainObject {
	private static final long serialVersionUID = 1L;	
	private String name;
	private Date startDate;
	private Date endDate;
	private double factor;
	
	public Discount (String name, Date startDate, Date endDate, double factor){
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.factor = factor;
	}	
}