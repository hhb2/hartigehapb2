package edu.avans.hartigehap.web.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.avans.hartigehap.domain.Discount;
import edu.avans.hartigehap.domain.Employee;
import edu.avans.hartigehap.domain.EmployeeComponent;
import edu.avans.hartigehap.domain.Manager;
import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.domain.Topping;
import edu.avans.hartigehap.repository.DiscountRepository;
import edu.avans.hartigehap.repository.EmployeeComponentRepository;
import edu.avans.hartigehap.repository.ToppingRepository;
import edu.avans.hartigehap.service.RestaurantPopulatorService;
import edu.avans.hartigehap.service.RestaurantService;


@Controller
public class RestaurantController {
		
	final Logger logger = LoggerFactory.getLogger(RestaurantController.class);
	
	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private RestaurantPopulatorService restaurantPopulatorService;
	@Autowired 
	private ToppingRepository toppingService;
	@Autowired
	private DiscountRepository discountService;
	@Autowired 
	private EmployeeComponentRepository employeeService;

	// mapping to "/" is not RESTful, but is for bootstrapping!
	@RequestMapping(value = {"/", "/restaurants"}, method = RequestMethod.GET)
	public String listRestaurants(Model uiModel) {
		Collection<Restaurant> restaurants = restaurantService.findAll();
		uiModel.addAttribute("restaurants", restaurants);
		// use HartigeHap as default restaurant
		Restaurant restaurant = restaurantService.fetchWarmedUp(RestaurantPopulatorService.HARTIGEHAP_RESTAURANT_NAME);
		uiModel.addAttribute("restaurant", restaurant);	
		return "hartigehap/listrestaurants";
	}

	@RequestMapping(value = "/restaurants/{restaurantName}", method = RequestMethod.GET)
	public String showRestaurant(@PathVariable("restaurantName") String restaurantName, Model uiModel) {

		// warmup stuff
		Collection<Restaurant> restaurants = restaurantService.findAll();
		uiModel.addAttribute("restaurants", restaurants);
		Restaurant restaurant = restaurantService.fetchWarmedUp(restaurantName);
		uiModel.addAttribute("restaurant", restaurant);
		
		return "hartigehap/restaurant";
	}
	
	
	//SHOW DISCOUNT
	@RequestMapping(value = "/restaurants/{restaurantName}/discounts", method = RequestMethod.GET)
	public String showDiscount(@PathVariable("restaurantName") String restaurantName, Model uiModel) {
		
		Collection<Discount> disCol = (Collection<Discount>) discountService.findAll();
		uiModel.addAttribute("discounts", disCol);
		Restaurant restaurant = restaurantService.fetchWarmedUp(restaurantName);
		uiModel.addAttribute("restaurant", restaurant);
		return "hartigehap/discounts";
	}
	
	
	@RequestMapping(value = "/restaurants/{restaurantName}/discounts", method = RequestMethod.POST)
	public String saveDiscount(@PathVariable("restaurantName") String restaurantName,
			@RequestParam("name") String name,
			@RequestParam("start") String start,
			@RequestParam("end") String end,
			@RequestParam("factor") Double factor,
			Model uiModel) {
		
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		Date startd;
		try {
			startd = format.parse(start);
			Date endd = format.parse(end);
			Discount d = new Discount(name, startd, endd, factor);
			discountService.save(d);	
		} catch (ParseException e) {
			
		}
		Collection<Discount> disCol = (Collection<Discount>) discountService.findAll();
		uiModel.addAttribute("discounts", disCol);
		Restaurant restaurant = restaurantService.fetchWarmedUp(restaurantName);
		uiModel.addAttribute("restaurant", restaurant);
		return "hartigehap/discounts";
	}
	
	//SAVE TOPPINGS
	@RequestMapping(value = "/restaurants/{restaurantName}/toppings", method = RequestMethod.POST)
	public String saveTopping(@PathVariable("restaurantName") String restaurantName, 
			@RequestParam("toppingName") String newTopping,
			@RequestParam("toppingPrice") Double toppingPrice,
			Model uiModel) {
		Collection<Topping> colTop = (Collection<Topping>) toppingService.findAll();
		uiModel.addAttribute("toppings", colTop);
		Topping t = new Topping();
		t.setPrice(toppingPrice);
		t.setName(newTopping);
		toppingService.save(t);
		Restaurant restaurant = restaurantService.fetchWarmedUp(restaurantName);
		uiModel.addAttribute("restaurant", restaurant);
		return "hartigehap/toppings";
	}
	
	//SHOW TOPPINGS
	@RequestMapping(value = "/restaurants/{restaurantName}/toppings", method = RequestMethod.GET)
	public String showToppings(@PathVariable("restaurantName") String restaurantName, Model uiModel) {
		Collection<Topping> colTop = (Collection<Topping>) toppingService.findAll();
		uiModel.addAttribute("toppings", colTop);
		Restaurant restaurant = restaurantService.fetchWarmedUp(restaurantName);
		uiModel.addAttribute("restaurant", restaurant);
		return "hartigehap/toppings";
	}
	
	@RequestMapping(value = "/restaurants/{restaurantName}/employees", method= RequestMethod.GET)
	public String showEmployees(@PathVariable("restaurantName") String restaurantName, Model uiModel){
		Collection<EmployeeComponent> empCol = (Collection<EmployeeComponent>) employeeService.findAll();
		uiModel.addAttribute("employees", empCol);
		
		return "hartigehap/employees";
	}
	
	@RequestMapping(value = "/restaurants/{restaurantName}/employees", method= RequestMethod.POST)
	public String saveEmployee(@PathVariable("restaurantName") String restaurantName, 
			@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam("salary") Double salary,
			@RequestParam("role") String role,
			Model uiModel){
		EmployeeComponent ec;
		if(role == "manager"){
			ec = new Manager(firstName, lastName, salary, role, false, false);
		}else{
			ec = new Employee(firstName, lastName, salary, role);
		}
		employeeService.save(ec);
		Collection<EmployeeComponent> empCol = (Collection<EmployeeComponent>) employeeService.findAll();
		uiModel.addAttribute("employees", empCol);
		
		return "hartigehap/employees";
	}

	// called once immediately after bean creation
	@PostConstruct
	public void createRestaurants() {
		restaurantPopulatorService.createRestaurantsWithInventory();
	}

	
}
