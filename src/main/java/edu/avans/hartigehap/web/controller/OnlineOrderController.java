package edu.avans.hartigehap.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.mysql.jdbc.log.Log;

import edu.avans.hartigehap.domain.Decorator;
import edu.avans.hartigehap.domain.FoodCategory;
import edu.avans.hartigehap.domain.Meal;
import edu.avans.hartigehap.domain.MenuItem;
import edu.avans.hartigehap.domain.OnlineOrder;
import edu.avans.hartigehap.domain.Pizza;
import edu.avans.hartigehap.domain.SimplePizza;
import edu.avans.hartigehap.domain.PizzaDecorator;
import edu.avans.hartigehap.domain.SimplePizza;
import edu.avans.hartigehap.domain.Topping;
import edu.avans.hartigehap.repository.FoodCategoryRepository;
import edu.avans.hartigehap.repository.MenuItemRepository;
import edu.avans.hartigehap.repository.OrderRepository;
import edu.avans.hartigehap.repository.ToppingRepository;
import edu.avans.hartigehap.service.OnlineOrderService;

@Controller
//@PreAuthorize("hasRole('ROLE_EMPLOYEE')")
public class OnlineOrderController {
	final Logger logger = LoggerFactory.getLogger(OnlineOrderController.class);
	
	//MAIN CONTROLLER METHODS 
	@Autowired
	private OnlineOrderService onlineOrderService;
	@Autowired
	private ToppingRepository toppingRepository;
	@Autowired
	private FoodCategoryRepository fcr;
	@Autowired
	private OrderRepository or;
	@Autowired
	private MenuItemRepository mir;
	
	@RequestMapping(value = "/restaurants/{restaurantName}/onlineorders/listmenu", method = RequestMethod.GET)
	public String listProducts(@PathVariable("restaurantName") String restaurantName, Model uiModel) {
		logger.info("Listing online order products");
		List<Topping> menuItems = onlineOrderService.findAllToppings();
		uiModel.addAttribute("menuItems", menuItems);
		logger.info("No. of items: " + menuItems.size());
		return "hartigehap/listmenu";
	}
	
	@RequestMapping(value = "/restaurants/{restaurantName}/onlineorders/customerinfo", method = RequestMethod.POST)
	public String addOrder(@PathVariable("restaurantName") String restaurantName, @RequestParam("sendData") String sendData, Model uiModel) 
	{
		uiModel.addAttribute("prod", sendData);
		return "hartigehap/customerinfo";
	}
	
	@RequestMapping(value = "/restaurants/{restaurantName}/onlineorders/confirmorder", method = RequestMethod.POST)
	public String confirmOrder(
			@PathVariable("restaurantName") String restaurantName, 
			@RequestParam("sendData") String sendData,
			@RequestParam("nm") String naam,
			@RequestParam("ad") String adres,
			@RequestParam("ps") String postcode,
			@RequestParam("wp") String woonplaats,
			Model uiModel)
	{
		String[] prods = sendData.split(",");
		List<Topping> list = (List<Topping>) toppingRepository.findAll();
		List<Topping> orderedToppings = new ArrayList<Topping>();
		//double totalPrice = 0.00;
		
		//orderedToppings.add(new Topping("Test topping", 7.00));
		for(int i = 0; i < prods.length; i++){
			if(prods[i] != ""){
				for(final Topping topping : list){
					System.out.println("Huidige topping: " + prods[i]+ " - Huidige db topping:  " + topping.getName());
					if((prods[i].toLowerCase()).contains((topping.getName().toLowerCase()))){
						orderedToppings.add(topping);
						//totalPrice = totalPrice + topping.getPrice();
					}
				}
			}
		}
		
		System.out.println("Topping objects: "+orderedToppings.size());
		
		Pizza decoratedPizza =  new SimplePizza(prods[0], "pizza.jpg", 7.50, fcr.findAll(), sendData);
		for(final Topping topping : orderedToppings){
			decoratedPizza = new Decorator(topping, decoratedPizza);
		} 
		
		System.out.println("TEST PRICE: " + decoratedPizza.getPrice());
		MenuItem meal = new Meal(decoratedPizza.getId(), decoratedPizza.getImageFileName(), decoratedPizza.getPrice(), decoratedPizza.getFoodCategories(), decoratedPizza.getRecipe());
		mir.save(meal);
		
		OnlineOrder o = new OnlineOrder();		
		MenuItem loadLatest = mir.findOne(prods[0]);
		o.addOrderItem(loadLatest);
		o.setCity(woonplaats);
		o.setZipCode(postcode);
		o.setStreet(adres);
		or.save(o);
		
		uiModel.addAttribute("sendInfo", naam + " <br>" + adres + "<br> " + postcode + " " + woonplaats);
		return "hartigehap/confirmorder";
	}
	
	@RequestMapping(value = "/restaurants/{restaurantName}/onlineorders/dynloadproducts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<FoodCategory> dynloadproducts(
			 HttpServletResponse httpResponse,
		        WebRequest httpRequest){
		return onlineOrderService.findAllFoodCategories();
	}
	
	
}
