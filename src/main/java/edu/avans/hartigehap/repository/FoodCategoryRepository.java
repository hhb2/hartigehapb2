package edu.avans.hartigehap.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import edu.avans.hartigehap.domain.FoodCategory;

public interface FoodCategoryRepository extends PagingAndSortingRepository<FoodCategory, String> {
	List<FoodCategory> findAll();
}
