package edu.avans.hartigehap.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import edu.avans.hartigehap.domain.Topping;

public interface ToppingRepository extends PagingAndSortingRepository<Topping, Long> {

}
