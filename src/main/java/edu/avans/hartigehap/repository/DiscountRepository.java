package edu.avans.hartigehap.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import edu.avans.hartigehap.domain.Discount;

public interface DiscountRepository extends PagingAndSortingRepository<Discount, Long> {

}
