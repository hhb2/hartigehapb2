package edu.avans.hartigehap.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import edu.avans.hartigehap.domain.EmployeeComponent;

public interface EmployeeComponentRepository extends PagingAndSortingRepository<EmployeeComponent, Long> {

}
