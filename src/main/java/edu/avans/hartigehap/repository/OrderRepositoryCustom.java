package edu.avans.hartigehap.repository;

import java.util.List;

import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.Restaurant;

public interface OrderRepositoryCustom {

	List<Order> findSubmittedOrdersForRestaurant(Restaurant restaurant);
}
