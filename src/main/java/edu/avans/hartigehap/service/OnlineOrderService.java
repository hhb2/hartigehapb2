package edu.avans.hartigehap.service;

import java.util.List;

import edu.avans.hartigehap.domain.FoodCategory;
import edu.avans.hartigehap.domain.MenuItem;
import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.Topping;



public interface OnlineOrderService {

	//Interface voor OnlineOrderServices Implementatie
	List<MenuItem> findAll();
	List<FoodCategory> findAllFoodCategories();
	void addOrder(Order o);
	
	public List<Topping> findAllToppings();

}
