package edu.avans.hartigehap.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.avans.hartigehap.domain.FoodCategory;
import edu.avans.hartigehap.domain.MenuItem;
import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.StateException;
import edu.avans.hartigehap.domain.Topping;
import edu.avans.hartigehap.repository.FoodCategoryRepository;
import edu.avans.hartigehap.repository.MenuItemRepository;
import edu.avans.hartigehap.repository.OrderRepository;
import edu.avans.hartigehap.repository.ToppingRepository;
import edu.avans.hartigehap.service.OnlineOrderService;

@Service("onlineOrderService")
@Repository
@Transactional(rollbackFor = StateException.class)
public class OnlineOrderServiceImpl implements OnlineOrderService {
	final Logger logger = LoggerFactory.getLogger(BillServiceImpl.class);
	
	@Autowired
	private MenuItemRepository menuItemRespository;
	@Autowired
	private OrderRepository orderRepo;
	@Autowired
	private FoodCategoryRepository fcRepo;
	@Autowired 
	private ToppingRepository toppingRepo;
	
	@Transactional(readOnly=true)
	public List<Topping> findAllToppings(){
		List<Topping> toppingList = (List<Topping>) toppingRepo.findAll();
		return toppingList;
	}
	
	@Transactional(readOnly=true)
	public List<MenuItem> findAll() {
		//Query all menuItems
		List<MenuItem> allMenuItems = menuItemRespository.findAll();
		return allMenuItems;

	}	
	
	@Transactional(readOnly=true)
	public List<FoodCategory> findAllFoodCategories(){
		return (fcRepo.findAll()); 
	}
	
	
	@Transactional(readOnly=false)
	public void addOrder(Order o)
	{
		orderRepo.save(o);
	}
	
	
}
